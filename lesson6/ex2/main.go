package main

import (
	"fmt"
)

func main() {

	fmt.Println("Задание 2. Сумма двух чисел по единице")

	var numberA, numberB int
	fmt.Println("Введите первое число:")
	fmt.Scan(&numberA)
	fmt.Println("Введите второе число:")
	fmt.Scan(&numberB)

	summAB := numberA + numberB

	for i := numberA; i < summAB; i++ {
		fmt.Println("Число =", i)
	}

}

/*
Задание 2. Сумма двух чисел по единице


Что нужно сделать
Напишите программу, которая запрашивает у пользователя два числа и складывает их в цикле следующим образом: берёт первое число и прибавляет к нему по единице, пока не достигнет суммы двух чисел.
*/
