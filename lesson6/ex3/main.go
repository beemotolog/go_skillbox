package main

import (
	"fmt"
)

func main() {

	fmt.Println("Задание 3. Расчёт суммы скидки")

	var price, discount, discountPrice float64
	fmt.Println("Введите стоимость товара:")
	fmt.Scan(&price)

	for true {
		fmt.Println("Введите скидку:")
		fmt.Scan(&discount)
		if discount > 30 {
			fmt.Println("Скидка не может быть больше 30%, повторите.")
			continue
		}
		discountPrice = price * discount * 0.01
		if discountPrice > 2000 {
			fmt.Println("Скидка не может быть больше 2000 руб, повторите.")
			continue
		}

		fmt.Println("Скидка составила:", discountPrice)
		break
	}

}

/*
Задание 3. Расчёт суммы скидки


Что нужно сделать
Напишите программу, которая принимает на вход цену товара и скидку. Посчитайте и верните на экран сумму скидки. Скидка должна быть не больше 30% от цены товара и не больше 2000 рублей.

*/
