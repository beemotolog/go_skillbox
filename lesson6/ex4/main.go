package main

import "fmt"

func main() {
	fmt.Println("Задание 4. Предыдущее занятие на if")
	firstVal := 0
	secondVal := 0
	thirdVal := 0
	firstMax := 10
	secondMax := 100
	thirdMax := 1000

	for true {
		if thirdVal == thirdMax {
			break
		}
		thirdVal++
		if secondVal == secondMax {
			continue
		}
		secondVal++
		if firstVal == firstMax {
			continue
		}
		firstVal++
	}

	fmt.Println("Цикл завершен")
	fmt.Println("Первая переменная:", firstVal)
	fmt.Println("Вторая переменная:", secondVal)
	fmt.Println("Третья переменная:", thirdVal)

}

/*

Задание 4. Предыдущее занятие на if


Что нужно сделать
Есть три переменные со значениями 0. Первую нужно наполнить до 10, вторую — до 100, третью — до 1000. Напишите цикл, в котором эти переменные будут увеличиваться на один.



Рекомендация
Используйте условия для пропуска переменных, которые уже достигли своих лимитов.

*/
