package main

import "fmt"

func main() {
	fmt.Println("Задание 6. Движение лифта")

	liftFloor := 1
	maxFloors := 24
	liftDirection := "вверх"
	passengersFourFloor := 3
	passengersSevenFloor := 3
	passengersTenFloor := 3
	passengersInLift := 0

	for true {
		fmt.Print("Лифт находится на ", liftFloor, " этаже\n")
		fmt.Println("Лифт движется ", liftDirection)
		fmt.Println("К-во пассажиров в лифте: ", passengersInLift)
		if liftDirection == "вверх" {
			liftFloor++
			if liftFloor == maxFloors {
				liftDirection = "вниз"
			}
		} else {
			liftFloor--
			if liftFloor == 1 {
				liftDirection = "вверх"
			}
		}

		if liftFloor == 1 && passengersInLift != 0 {
			fmt.Println("Из лифта вышло пассажиров: ", passengersInLift)
			fmt.Println("Ожидают пассажиров на 4-м этаже:", passengersFourFloor)
			fmt.Println("Ожидают пассажиров на 7-м этаже:", passengersSevenFloor)
			fmt.Println("Ожидают пассажиров на 10-м этаже:", passengersTenFloor)
			passengersInLift = 0
		}

		if liftFloor == 4 && passengersFourFloor != 0 && liftDirection == "вниз" {
			if passengersInLift == 2 {
				fmt.Println("Лифт полный и не может взять пассажиров")
			} else if passengersInLift == 1 {
				passengersFourFloor--
				passengersInLift++
				fmt.Println("В лифт зашел 1 пассажир")
			} else {
				if passengersFourFloor > 2 {
					passengersInLift += 2
					passengersFourFloor -= 2
					fmt.Println("В лифт зашло 2 пассажира")
				} else {
					passengersInLift = passengersFourFloor
					passengersFourFloor = 0
					fmt.Println("В лифт зашел 1 пассажир")
				}
			}
		}

		if liftFloor == 7 && passengersSevenFloor != 0 && liftDirection == "вниз" {
			if passengersInLift == 2 {
				fmt.Println("Лифт полный и не может взять пассажиров")
			} else if passengersInLift == 1 {
				passengersSevenFloor--
				passengersInLift++
				fmt.Println("В лифт зашел 1 пассажир")
			} else {
				if passengersSevenFloor > 2 {
					passengersInLift += 2
					passengersSevenFloor -= 2
					fmt.Println("В лифт зашло 2 пассажира")
				} else {
					passengersInLift = passengersSevenFloor
					passengersSevenFloor = 0
					fmt.Println("В лифт зашел 1 пассажир")
				}
			}
		}

		if liftFloor == 10 && passengersTenFloor != 0 && liftDirection == "вниз" {
			if passengersInLift == 2 {
				fmt.Println("Лифт полный и не может взять пассажиров")
			} else if passengersInLift == 1 {
				passengersTenFloor--
				passengersInLift++
				fmt.Println("В лифт зашел 1 пассажир")
			} else {
				if passengersTenFloor > 2 {
					passengersInLift += 2
					passengersTenFloor -= 2
					fmt.Println("В лифт зашло 2 пассажира")
				} else {
					passengersInLift = passengersTenFloor
					passengersTenFloor = 0
					fmt.Println("В лифт зашел 1 пассажир")
				}
			}
		}

		if passengersInLift == 0 && passengersFourFloor == 0 &&
			passengersSevenFloor == 0 && passengersTenFloor == 0 {
			break
		}

	}

	fmt.Println("Ура! Лифт перевез всех пассажиров.")
}

/*

Задание 6 (по желанию). Движение лифта


Что нужно сделать
В доме — 24 этажа. Лифт должен ходить вверх-вниз, пока не доставит всех пассажиров на первый этаж.
Три пассажира ждут на четвёртом, седьмом и десятом этажах.
При движении вверх лифт не должен останавливаться, при движении вниз — должен собирать всех, но не более двух человек в лифте. При этом лифт каждый раз доезжает до самого верхнего этажа и только после этого начинает движение вниз. Напишите программу, которая доставит всех пассажиров на первый этаж.



*/
