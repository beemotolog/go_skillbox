package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Задание 1. Зеркальные билеты")

	startNum := 100000
	endNum := 999999
	fmt.Println("Количество зеркальных билетов среди всех шестизначных номеров")
	fmt.Printf("от %d до %d:\n", startNum, endNum)
	var happyTickets, ticketNum, reverceNum int

	for ticket := startNum; ticket <= endNum; ticket++ {
		ticketNum = ticket
		reverceNum = 0
		for digit := 6; digit >= 1; digit-- {
			reverceNum += ticketNum % 10 * int(math.Pow(10, float64(digit)))
			ticketNum /= 10
		}
		if ticket == reverceNum {
			happyTickets++
		}
	}
	fmt.Println(happyTickets)

}

/*
Задание 1. Зеркальные билеты


Что нужно сделать
Выведите, сколько зеркальных билетов находится среди всех шестизначных номеров от 100000 до 999999.



Советы и рекомендации
Для зеркального отображения числа используйте цикл.

Пример работы программы:

Количество зеркальных билетов среди всех шестизначных номеров
от 100000 до 999999:
900
*/
