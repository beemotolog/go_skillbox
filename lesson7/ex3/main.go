package main

import "fmt"

func main() {

	fmt.Println("Задание 3. Вывод ёлочки")

	var height int
	fmt.Println("Введите высоту ёлочки:")
	fmt.Scan(&height)

	for y := 0; y < height; y++ {
		for x := 0; x < height-y-1; x++ {
			fmt.Print(" ")
		}
		for x := 0; x <= y*2; x++ {
			fmt.Print("*")
		}
		fmt.Println()
	}

}

/*

Задание 3. Вывод ёлочки


Что нужно сделать
Усложним задачу рисования: попробуйте вывести ёлочку. В первой строке выведите одну звёздочку, во второй — на две больше, в третьей — ещё на две больше, и так до количества строк, указанных пользователем.

Правила вывода ёлочки: она симметрична, количество строк соответствует введённому пользователем.



Советы и рекомендации
Необходимо сначала выводить пробелы, а затем — звёздочки. Посмотрите, как количество пробелов и звёздочек в каждой строке связано с введённым количеством строк и номером текущей строки. Внутри цикла по строкам, скорее всего, понадобятся два цикла: один — для вывода пробелов, второй — для вывода звёздочек.

Пример работы программы:

Вывод ёлочки.
Введите высоту ёлочки:
5
    *
   ***
  *****
 *******
*********

*/
