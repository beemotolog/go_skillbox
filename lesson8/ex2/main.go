package main

import "fmt"

func main() {

	fmt.Println("Задание 2. Дни недели")
	var dayOfWeek string
	fmt.Println("Введите будний день недели: пн, вт, ср, чт, пт:")
	fmt.Scan(&dayOfWeek)

	switch dayOfWeek {
	case "пн":
		fmt.Println("понедельник")
		dayOfWeek = "вт"
		fallthrough
	case "вт":
		fmt.Println("вторник")
		dayOfWeek = "ср"
		fallthrough
	case "ср":
		fmt.Println("среда")
		dayOfWeek = "чт"
		fallthrough
	case "чт":
		fmt.Println("четверг")
		dayOfWeek = "пт"
		fallthrough
	case "пт":
		fmt.Println("пятница")
	default:
		fmt.Println("Такого рабочего дня недели не существует.")
	}

}

/*

Задание 2. Дни недели


Что нужно сделать
Пользователь вводит будний день недели в сокращённой форме (пн, вт, ср, чт, пт) и получает развёрнутый список всех последующих рабочих дней, включая пятницу.



Рекомендация
Пример работы программы:

Дни недели.
Введите будний день недели: пн, вт, ср, чт, пт:
вт
вторник
среда
четверг
пятница

*/
